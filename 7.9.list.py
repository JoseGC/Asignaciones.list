# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
max, min = a.index(max(a)), a.index(min(a))
a[max], a[min] = a[min], a[max]

print(a)
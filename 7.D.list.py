# Read a list of integers:
n, k = [int(s) for s in input().split()]
# Print a value:
# print(a)
pin = ['I'] * n
for i in range(k):
  left, right = [int(s) for s in input().split()]
  for j in range(left -1, right):
    pin[j] = '.'
print (''.join(pin))
# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
m, n = [int(s) for s in input().split()]
for i in range(0, m):
  myList = list(map(int, input().split()))
  if i == 0:
    max_e = max(myList)
    maxlist = [0, myList.index(max(myList))]
  elif max(myList) > max_e:
    max_e = max(myList)
    maxlist = [i, myList.index(max(myList))]
print(*maxlist, sep=' ')